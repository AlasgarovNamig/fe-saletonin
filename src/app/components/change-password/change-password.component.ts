import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  newPasswordForm: FormGroup = this.formBuilder.group({
    // email: ["", Validators.compose([Validators.required, Validators.email])]
    password: ["", Validators.compose([Validators.required, Validators.minLength(8)])],
    confirmPassword: ["", Validators.required],
  }, { validator: this.checkIfMatchingPasswords('password', 'confirmPassword') })

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }
  newPassword() {

  }
}
