import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginDTO } from 'src/app/dto/loginDto';
import { faEnvelope, faUser } from "@fortawesome/free-solid-svg-icons";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // for password eye action
  visible: boolean = true
  changeType: boolean = true
  // checked: boolean = true

  //for animation get container div 
  conatiner: any;

  //for request data object
  loginDto !: LoginDTO

  //for form validatiors create
  loginForm: FormGroup = this.formBuilder.group({
    email: ["", Validators.compose([Validators.required, Validators.email])],
    password: ["", Validators.compose([Validators.required, Validators.minLength(8)])],
    rememberMe: [true, Validators.required]

  })
  forgotPasswordForm: FormGroup = this.formBuilder.group({
    email: ["", Validators.compose([Validators.required, Validators.email])]
  })

  constructor(private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.conatiner = document.getElementById("container");
    this.conatiner.classList.add('login')
  }

  login() {
    if (this.loginForm.valid) {
      console.log(this.loginForm)
    } else {
      console.log("valid deyil !!!")
    }
  }
  forgotPassword() {

    if (this.forgotPasswordForm.valid) {
      console.log(this.forgotPasswordForm)
    } else {
      console.log('valid deyil !!!')
    }
  }

  toggle() {
    this.conatiner.classList.toggle('login')
    this.conatiner.classList.toggle('forgot-password')
  }

  viewPass() {
    this.visible = !this.visible
    this.changeType = !this.changeType
  }
}
