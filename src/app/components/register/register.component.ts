import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginDTO } from 'src/app/dto/loginDto';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  companyTypes: any = [
    {
      "id": 0,
      "name": "SINGLE SECTOR BUSINESS"
    },
    {
      "id": 1,
      "name": "MULTI SECTOR BUSINESS"
    }
  ]

  shopBusinessTypes: any = [
    {
      "id": 0,
      "name": "CLOTHES"
    },
    {
      "id": 1,
      "name": "FOOD"
    },
    {
      "id": 2,
      "name": "ELECTRONICS"
    }
  ]

  //for animation get container div 
  conatiner: any;

  //for form validatiors create
  registerOne: FormGroup = this.formBuilder.group({
    companyName: ["", Validators.required],
    companyType: [this.companyTypes.name, Validators.required],
    shopBusinessType: ["", Validators.required]
  })
  registerTwo: FormGroup = this.formBuilder.group({
    name: ["", Validators.compose([Validators.required, Validators.minLength(2)])],
    surname: ["", Validators.compose([Validators.required, Validators.minLength(2)])],
    email: ["", Validators.compose([Validators.required, Validators.email])],
    password: ["", Validators.compose([Validators.required, Validators.minLength(8)])],
    confirmPassword: ["", Validators.required],
  }, { validator: this.checkIfMatchingPasswords('password', 'confirmPassword') })

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }


  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.conatiner = document.getElementById("container");
    this.conatiner.classList.add('register1')
  }

  register() {
    if (this.registerOne.valid && this.registerTwo.valid) {
      console.log(this.registerOne)
      console.log(this.registerTwo)
    } else {
      console.log("invalid !!!!")
    }

  }

  toggle() {
    this.conatiner.classList.toggle('register1')
    this.conatiner.classList.toggle('register2')
  }

}
