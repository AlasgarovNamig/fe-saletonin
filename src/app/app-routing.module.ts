import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { CreatePasswordComponent } from './components/create-password/create-password.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { VerifyComponent } from './components/verify/verify.component';

const routes: Routes = [
  {path:'auth/login',component:LoginComponent},
  {path:'auth/register',component:RegisterComponent},
  {path:'auth/verify',component:VerifyComponent},
  {path:'auth/create-password',component:CreatePasswordComponent},
  {path:'auth/forgot-password',component:ForgotPasswordComponent},
  {path:'auth/change-password',component:ChangePasswordComponent},
  {path:'',redirectTo:'/auth/login',pathMatch:'full'},
  {path:'auth',redirectTo:'/auth/login',pathMatch:'full'},
  {path:'**',component:NotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
